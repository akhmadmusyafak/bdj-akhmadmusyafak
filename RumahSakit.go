package main

import ("fmt"
	"log"
	"net/http"
	"encoding/json")
	
type Location struct{
	Latitude float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Kelurahan struct{
	Kode string `json:"kode"`
	Nama string `json:"nama"`
}

type Kecamatan struct{
	Kode string `json:"kode"`
	Nama string `json:"nama"`
}

type Kota struct{
	Kode string `json:"kode"`
	Nama string `json:"nama"`
}

type RumahSakit struct{
	Id int `json:"id"`
	NamaRsu string `json:"nama_rsu"`
	JenisRsu string `json:"jenis_rsu"`
	Location `json:"location"`
	Alamat string `json:"alamat"`
	KodePos int `json:"int"`
	Telepon []string `json:"telepon"`
	Faksimile []string `json:"faksimile"`
	Website string `json:"website"`
	Email string `json:"email"`
	Kelurahan `json:"kelurahan"`
	Kecamatan `json:"kecamatan"`
	Kota `json:"kota"`
}

var RumahSakits []RumahSakit

func returnAllRumahSakit(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: returnAllRumahSakit")
    json.NewEncoder(w).Encode(RumahSakits)
}

func handleRequests() {
    
    http.HandleFunc("/rumahsakitumum-kelurahan", returnAllRumahSakit)
    log.Fatal(http.ListenAndServe(":10000", nil))
}

func main(){
	
	RumahSakits = []RumahSakit{
		RumahSakit{2, "Tarakan", "Rumah Sakit Umum Daerah", Location{Latitude: -6.171333, Longitude: 106.810013}, "Jl.Kyai Caringin No. 7", 10150,
		[]string{"3503150","3503003","3508993"}, []string{"3503412","3863309",}, "www.rstarakanjakarta.com",
		"kusmedi@gmail.com, rsd_tarakan@yahoo.com", Kelurahan{"3173080001","Bendungan Hilir",}, 
		Kecamatan{"3173080","Tanah Abang",}, Kota{"3173","Jakarta Pusat",}},
	}
	handleRequests()
}	